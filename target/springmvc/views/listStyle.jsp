<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>"
	type="image/x-icon">
<link rel="icon" href="<c:url value="/images/favicon.ico"/>"
	type="image/x-icon">

<title>Yudi Games</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">


<!-- Custom CSS -->
<link rel="stylesheet" href="<c:url value="/css/shop-homepage.css"/>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a href="/springmvc/"> <img alt="Yudi Games"
					src="<c:url value="images/icone.png"/>">
				</a>
			</div>
			</br> </br>
			<div style="text-align: right;">
				<a href="createAccount" style="color: #fff">Criar conta</a> <span
					style="color: #fff">|</span> <a href="premiumAccount"
					style="color: #fff">Conta Premium</a>
			</div>

		</div>
		<!-- /.container -->
	</nav>

	<!-- Page Content -->
	<div class="container">

		<div class="row">

			<div class="col-md-3">
				<br> <br>
				<p class="lead">Categorias</p>
				<div class="list-group">
					<a href="<c:url value="listProductToSite/RPG"/>"
						class="list-group-item">RPG</a> <a
						href="<c:url value="listProductToSite/ACAO"/>"
						class="list-group-item">Ação</a> <a
						href="<c:url value="listProductToSite/OUTROS"/>"
						class="list-group-item">Outros</a>
				</div>
			</div>

			<div class="col-md-9">

				<div class="row carousel-holder">

					<div class="col-md-12">
						<div id="carousel-example-generic" class="carousel slide"
							data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carousel-example-generic" data-slide-to="0"
									class="active"></li>
								<li data-target="#carousel-example-generic" data-slide-to="1"></li>
								<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="item active">
									<a href="#"> <img class="slide-image"
										src="http://rafaeltome.dominiotemporario.com/scrummaster/banner/final_fantasy.png"
										alt="">
									</a>
								</div>
								<div class="item">
									<a href="#"> <img class="slide-image"
										src="http://rafaeltome.dominiotemporario.com/scrummaster/banner/street_figther.png"
										alt="">
									</a>
								</div>
								<div class="item">
									<a href="#"> <img class="slide-image"
										src="http://rafaeltome.dominiotemporario.com/scrummaster/banner/tomb_raider.png"
										alt="">
									</a>
								</div>
							</div>
							<a class="left carousel-control" href="#carousel-example-generic"
								data-slide="prev"> <span
								class="glyphicon glyphicon-chevron-left"></span>
							</a> <a class="right carousel-control"
								href="#carousel-example-generic" data-slide="next"> <span
								class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</div>
					</div>

				</div>

				<div class="row">

					<c:forEach var="product" items="${productList}">
						<div class="col-sm-4 col-lg-4 col-md-4">
							<div class="thumbnail">
								<img
									src="http://rafaeltome.dominiotemporario.com/scrummaster/${product.url}">
								<div class="caption">
									<h4 class="pull-right">${product.price}</h4>
									<h4>
										<a href="#">${product.name}</a>
									</h4>
									<p>${product.description}
									<div>
										<a href="gift/${product.id}">
											<button type="button" class="btn btn-default pull-right">
												<img alt="Yudi Games" src="images/gift.png" width="16"
													height="16" />
											</button>
										</a>
										<form target="pagseguro" method="post"
											action="https://pagseguro.uol.com.br/checkout/checkout.jhtml">
											<input type="hidden" name="email_cobranca"
												value="rafaeltsouza@terra.com.br" /> <input type="hidden"
												name="tipo" value="CBR" /> <input type="hidden"
												name="moeda" value="BRL" /> <input type="hidden"
												name="item_id" value="${product.id}" /> <input
												type="hidden" name="item_descr" value="${product.name}" />
											<input type="hidden" name="item_quant" value="1" /> <input
												type="hidden" name="item_valor"
												value="${product.priceWithoutFormat}" /> <input
												type="hidden" name="frete" value="0" /> <input
												type="hidden" name="peso" value="0" /> <input type="image"
												name="submit"
												src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-comprar-assina.gif"
												alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
										</form>

										<!-- 
										<button type="button" class="btn btn-default pull-right" aria-label="Left Align">
											<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> 
											<a href="#">Comprar</a>
										</button>  
										<button type="button" class="btn btn-default pull-right">
											<a href="gift/${product.id}"><img alt="Yudi Games" src="images/gift.png" width="16" height="16"></a>
										</button>
										 -->

									</div>
									</p>
								</div>
							</div>
						</div>
					</c:forEach>

				</div>

			</div>

		</div>
	</div>

	</div>
	<!-- /.container -->

	<div class="container">

		<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="container">
					<p>Copyright &copy; Yudi Games 2015</p>
				</div>
			</div>
		</footer>

	</div>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="<c:url value="/js/jquery.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
</body>

</html>