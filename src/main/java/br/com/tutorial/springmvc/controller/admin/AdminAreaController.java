package br.com.tutorial.springmvc.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminAreaController {

	@RequestMapping("/admin/")
	public String redirect() {
		return "indexAdmin";
	}
	
}
