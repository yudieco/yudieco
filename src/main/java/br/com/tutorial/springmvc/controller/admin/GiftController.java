package br.com.tutorial.springmvc.controller.admin;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.tutorial.springmvc.dao.DAOGift;
import br.com.tutorial.springmvc.dao.DAOProduct;
import br.com.tutorial.springmvc.model.Gift;
import br.com.tutorial.springmvc.model.Product;

@Controller
public class GiftController {

	public static String EMPTY_VALUE = "NULLVALUE";

	public static String USERNAME_EXIST = "USERNAMEEXIST";

	public static String CORRECT = "OK";

	@Autowired
	private DAOGift daoGift;
	
	@Autowired
	private DAOProduct daoProduct;

	@ModelAttribute("giftBuy")
	public Gift create() {
		return new Gift();
	}

	@RequestMapping(value = "/gift", method = RequestMethod.GET)
	public ModelAndView giftToSomebody(
			@ModelAttribute("giftBuy") Gift gift) {
		ModelAndView m = new ModelAndView();
		m.setViewName("gift");
		return m;
	}

	@RequestMapping(value = "/gift/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, @ModelAttribute("giftBuy") Gift giftBuy,
			RedirectAttributes redirectAttributes) {
		Product product = daoProduct.get(id);
		redirectAttributes.addFlashAttribute("productRegistration", product);
		return "redirect:/gift";
	}

	@RequestMapping(value = "/gift", method = RequestMethod.POST)
	public String add(@Valid Gift gift, BindingResult result, Model model,
			Principal principal) {
		boolean go = false;
		if (gift.getCreditcard().isEmpty() || gift.getEmail().isEmpty()
				|| gift.getProduct() == null || gift.getSecureCode().isEmpty()) {
			go = false;
		} else {
			go = true;
		}

		if (!go) {
			return "redirect:/createEmptyAccount";
		} else {
			Product product = daoProduct.get(gift.getProduct().getId());
			gift.setProduct(product);
			daoGift.save(gift);
			return "redirect:/createMessageAccount";
		}
	}

}
