package br.com.tutorial.springmvc.controller.physician;

import java.lang.annotation.Retention;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping("/")
	public String index() {
		return "redirect:/listProductToSite/ACAO";
	}
	
	@RequestMapping("/createAccount")
	public String createAccount() {
		return "createAccount";
	}
	
	@RequestMapping("/createEmptyAccount")
	public String createEmptyAccount() {
		return "createEmptyAccount";
	}
	
	@RequestMapping("/createExistingAccount")
	public String createExistingAccount() {
		return "createExistingAccount";
	}

	@RequestMapping("/index")
	public String redirect() {
		return "index";
	}

	@RequestMapping("/createMessageAccount")
	public String createMessageAccount() {
		return "createMessageAccount";
	}

	@RequestMapping("/premiumAccount")
	public String premiumAccount() {
		return "premiumAccount";
	}
}
