package br.com.tutorial.springmvc.controller.admin;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.tutorial.springmvc.business.CustomerBO;
import br.com.tutorial.springmvc.model.Customer;

@Controller
public class AccountController {

	@Autowired
	private CustomerBO customerBO;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	@ModelAttribute("customerRegistration")
	public Customer create() {
		return new Customer();
	}

	@RequestMapping(value="/createAccount",method=RequestMethod.GET)
	public ModelAndView redirectedToAdd(@ModelAttribute("customerRegistration") Customer customer) {
		ModelAndView m = new ModelAndView();
		m.setViewName("createAccount");
		return m;
	}

	@RequestMapping(value="/createAccount",method=RequestMethod.POST)
	public String add(@Valid Customer customer, BindingResult result, Model model, Principal principal) {
		String position = customerBO.saveCustomer(customer);
		
		if (position.equals(CustomerBO.CORRECT)) {
			return "redirect:/createMessageAccount";
		} else if (position.equals(CustomerBO.EMPTY_VALUE)) {
			return "redirect:/createEmptyAccount";
		} else {
			return "redirect:/createExistingAccount";
		}
	}

	
}
