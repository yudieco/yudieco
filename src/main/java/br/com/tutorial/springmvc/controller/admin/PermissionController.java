package br.com.tutorial.springmvc.controller.admin;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.tutorial.springmvc.dao.DAOPermission;
import br.com.tutorial.springmvc.model.Permission;

@Controller
public class PermissionController {
	
	@Autowired
	private DAOPermission daoPermission;
	private List<Permission> listPermission;
	
	
	public PermissionController() {
		listPermission = new ArrayList<Permission>();
	}
	
	public DAOPermission getDaoPermission() {
		return daoPermission;
	}

	public void setDaoPermission(DAOPermission daoPermission) {
		this.daoPermission = daoPermission;
	}

	@ModelAttribute("permissionRegistration")
	public Permission create() {
		return new Permission();
	}

	@RequestMapping(value="/admin/listPermission")
	public ModelAndView redirectedToList() {
		ModelAndView m = new ModelAndView();
		m.setViewName("listPermission");
		listPermission  = daoPermission.getAll();
		m.addObject("permissionList", listPermission);
		return m;
	}

	@RequestMapping(value="/admin/addPermission",method=RequestMethod.GET)
	public ModelAndView redirectedToAdd(@ModelAttribute("permissionRegistration") Permission permission) {
		ModelAndView m = new ModelAndView();
		m.setViewName("addPermission");
		return m;
	}

	@RequestMapping(value="/admin/addPermission",method=RequestMethod.POST)
	public String add(@Valid Permission permission, BindingResult result, Model model) {
		daoPermission.save(permission);
		return "redirect:/admin/listPermission";
	}
	
	@RequestMapping(value="/admin/deletePermission/{id}",method=RequestMethod.GET)
	public String delete(@PathVariable("id") long id) {
		daoPermission.delete(id);
		return "redirect:/admin/listPermission";
	}

	@RequestMapping(value="/admin/editPermission/{id}",method=RequestMethod.GET)
	public String edit(@PathVariable("id") long id, RedirectAttributes redirectAttributes) {
		Permission permission = daoPermission.get(id);
		redirectAttributes.addFlashAttribute("permissionRegistration",permission);
		return "redirect:/admin/addPermission";
	}

	@RequestMapping(value="/admin/editPermission/{id}",method=RequestMethod.POST)
	public String update(@Valid Permission permission, BindingResult result, Model model) {
		daoPermission.save(permission);
		return "redirect:/admin/listPermission";
	}
	
}
