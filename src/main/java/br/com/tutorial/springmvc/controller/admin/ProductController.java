package br.com.tutorial.springmvc.controller.admin;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.tutorial.springmvc.dao.DAOProduct;
import br.com.tutorial.springmvc.model.Product;

@Controller
public class ProductController {
	
	@Autowired
	private DAOProduct daoProduct;
	private List<Product> listProduct;
	
	
	public ProductController() {
		listProduct = new ArrayList<Product>();
	}
	
	public DAOProduct getDaoProduct() {
		return daoProduct;
	}

	public void setDaoProduct(DAOProduct daoProduct) {
		this.daoProduct = daoProduct;
	}

	@ModelAttribute("productRegistration")
	public Product create() {
		return new Product();
	}

	@RequestMapping(value="/admin/listProduct")
	public ModelAndView redirectedToList() {
		ModelAndView m = new ModelAndView();
		m.setViewName("listProduct");
		listProduct  = daoProduct.getAll();
		m.addObject("productList", listProduct);
		return m;
	}

	@RequestMapping(value="/listProductToSite",method=RequestMethod.GET)
	public String edit() {
		return "listProductToSite";
	}

	@RequestMapping(value="/listProductToSite/{style}",method=RequestMethod.GET)
	public String edit(@PathVariable("style") String id, RedirectAttributes redirectAttributes) {
		listProduct  = daoProduct.getStyle(id);
		redirectAttributes.addFlashAttribute("productList",listProduct);
		return "redirect:/listProductToSite";
	}

	
	@RequestMapping(value="/admin/addProduct",method=RequestMethod.GET)
	public ModelAndView redirectedToAdd(@ModelAttribute("productRegistration") Product product) {
		ModelAndView m = new ModelAndView();
		m.setViewName("addProduct");
		return m;
	}

	@RequestMapping(value="/admin/addProduct",method=RequestMethod.POST)
	public String add(@Valid Product product, BindingResult result, Model model) {
		daoProduct.save(product);
		return "redirect:/admin/listProduct";
	}
	
	@RequestMapping(value="/admin/deleteProduct/{id}",method=RequestMethod.GET)
	public String delete(@PathVariable("id") long id) {
		daoProduct.delete(id);
		return "redirect:/admin/listProduct";
	}

	@RequestMapping(value="/admin/editProduct/{id}",method=RequestMethod.GET)
	public String edit(@PathVariable("id") long id, RedirectAttributes redirectAttributes) {
		Product product = daoProduct.get(id);
		redirectAttributes.addFlashAttribute("productRegistration",product);
		return "redirect:/admin/addProduct";
	}

	@RequestMapping(value="/admin/editProduct/{id}",method=RequestMethod.POST)
	public String update(@Valid Product product, BindingResult result, Model model) {
		daoProduct.save(product);
		return "redirect:/admin/listProduct";
	}
	
}
