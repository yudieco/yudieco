package br.com.tutorial.springmvc.controller.physician;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

	@RequestMapping("/login")
	public String redirect() {
		return "login";
	}
	
	@RequestMapping("/loginfailed")
	public String logout() {
		return "loginfailed";
	}


}
