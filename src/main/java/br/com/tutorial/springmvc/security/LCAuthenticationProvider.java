package br.com.tutorial.springmvc.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import br.com.tutorial.springmvc.business.UserBO;
import br.com.tutorial.springmvc.model.Permission;
import br.com.tutorial.springmvc.model.User;

public class LCAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired 
	private UserBO userBO;

	public Authentication authenticate(Authentication auth)	throws AuthenticationException {
		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) auth;
		String username = token.getName();
		String password = token.getCredentials() != null ? token.getCredentials().toString() : null;

		Object user = userBO.isValidUser(username, DigestUtils.sha256Hex(password));
		if (user == null) {
			return null;
		}

		List<Permission> permissoes = new ArrayList<Permission>();
		Permission p = ((User)user).getPermission();
		permissoes.add(p);
		
		LCAuthentication resultado = new LCAuthentication(((User)user), permissoes);
		resultado.setAuthenticated(user != null);
		return resultado;
	}

	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
