package br.com.tutorial.springmvc.security;


import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import br.com.tutorial.springmvc.model.Permission;
import br.com.tutorial.springmvc.model.User;

public class LCAuthentication implements Authentication{

	private static final long serialVersionUID = 1L;
	private final User user;
	private boolean authenticated;
	private List<Permission> grant;
	
	public LCAuthentication(User user, List<Permission> grant) {
		this.user = user;
		this.grant = grant;
	}
	
	public String getName() {
		return user.getName() != null ? user.getUsername() : null;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return grant;
	}

	public Object getCredentials() {
		return user.getName() != null ? user.getPassword() : null;
	}

	public Object getDetails() {
		return user;
	}

	public Object getPrincipal() {
		return user.getName() != null ? user.getUsername() : null;
	}
	
	public boolean isAuthenticated() {
		return this.authenticated;
	}

	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}	
}