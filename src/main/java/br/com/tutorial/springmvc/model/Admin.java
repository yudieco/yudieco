package br.com.tutorial.springmvc.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="admin")
public class Admin extends User {
	
	private long register;

	public long getRegister() {
		return register;
	}
	public void setRegister(long register) {
		this.register = register;
	}

}
