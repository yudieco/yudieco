package br.com.tutorial.springmvc.dao;

import java.util.List;

import br.com.tutorial.springmvc.model.Permission;

public interface DAOPermission extends DAOBase<Permission>{

	public Permission get(String name);
	public void delete(long id);
	public List<Permission> getAll();
	
}
