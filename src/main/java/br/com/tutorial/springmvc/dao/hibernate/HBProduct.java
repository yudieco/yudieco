package br.com.tutorial.springmvc.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tutorial.springmvc.dao.DAOProduct;
import br.com.tutorial.springmvc.model.Product;



@Transactional(propagation=Propagation.SUPPORTS)
@Repository("daoProduct")
public class HBProduct extends HBDAO<Product> implements DAOProduct {

	@Override
	protected Class<Product> getClazz() {
		return Product.class;
	}

	public Product get(int id) {
		Query query = getSession().createQuery("from Product where id = ? ");
		query.setInteger(0, id);
		return (Product) query.uniqueResult();				   
	}		

	public void delete(long id) {
		Query query = getSession().createQuery("from Product where id = ?");
		query.setLong(0, id);
		getSession().delete((Product)query.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<Product> getAll() {
		Query query = getSession().createQuery("from Product order by 'asc'");
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Product> getStyle(String style) {
		Query query = getSession().createQuery("from Product where style = '"+style+"' order by 'asc'");
		return query.list();
	}

}
