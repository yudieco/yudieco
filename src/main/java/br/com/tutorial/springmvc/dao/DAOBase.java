package br.com.tutorial.springmvc.dao;

import java.util.List;

public interface DAOBase<T> {
	
	public void save(T object);
	public void delete(T object);
	public T get(Long id);
	public List<T> list();
	
}
