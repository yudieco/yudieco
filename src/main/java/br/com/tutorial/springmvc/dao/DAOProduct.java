package br.com.tutorial.springmvc.dao;

import java.util.List;

import br.com.tutorial.springmvc.model.Product;

public interface DAOProduct extends DAOBase<Product> {

	public Product get(int id);
	public void delete(long id);
	public List<Product> getAll();
	public List<Product> getStyle(String id);

}
