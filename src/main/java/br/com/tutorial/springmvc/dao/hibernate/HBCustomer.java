package br.com.tutorial.springmvc.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tutorial.springmvc.dao.DAOCustomer;
import br.com.tutorial.springmvc.model.Customer;

@Transactional(propagation=Propagation.SUPPORTS)
@Repository("daoCustomer")
public class HBCustomer extends HBDAO<Customer> implements DAOCustomer{

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return Customer.class;
	}

	public boolean usernameIsValid(String username) {
		if (get(username) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public Customer get(String username) {
		Query query = getSession().createQuery("from Customer where username = ?");
		query.setString(0, username);
		return (Customer) query.uniqueResult();				   
	}	
	
	public Customer get(String username, String password) {
		Query query = getSession().createQuery("from Customer where username = ? and password = ?");
		query.setString(0, username);
		query.setString(1, DigestUtils.sha256Hex(password));
		query.setString(1,password);
		return (Customer) query.uniqueResult();				   
	}
	
	public void delete(long id) {
		Query query = getSession().createQuery("from Customer where id = ?");
		query.setLong(0, id);
		getSession().delete((Customer) query.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getAll() {
		Query query = getSession().createQuery("from Customer where customer order by 'asc'");
		if (query.list().isEmpty()) {
			List<Customer> list = new ArrayList<Customer>();
			list = null;
			return list;
		} else {
			return query.list();
		}
	}
	
}
