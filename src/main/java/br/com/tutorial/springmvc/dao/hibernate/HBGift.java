package br.com.tutorial.springmvc.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tutorial.springmvc.dao.DAOGift;
import br.com.tutorial.springmvc.model.Gift;
import br.com.tutorial.springmvc.model.Product;



@Transactional(propagation=Propagation.SUPPORTS)
@Repository("daoGift")
public class HBGift extends HBDAO<Gift> implements DAOGift {

	@Override
	protected Class<Gift> getClazz() {
		return Gift.class;
	}

	public Gift get(int id) {
		Query query = getSession().createQuery("from Gift where id = ? ");
		query.setInteger(0, id);
		return (Gift) query.uniqueResult();				   
	}		

	public void delete(long id) {
		Query query = getSession().createQuery("from Gift where id = ?");
		query.setLong(0, id);
		getSession().delete((Product)query.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<Gift> getAll() {
		Query query = getSession().createQuery("from Gift order by 'asc'");
		return query.list();
	}


}
