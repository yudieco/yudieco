package br.com.tutorial.springmvc.dao;

import java.util.List;

import br.com.tutorial.springmvc.model.Admin;
import br.com.tutorial.springmvc.model.User;

public interface DAOAdmin extends DAOBase<Admin>{

	public Admin get(String username, String password);
	public void delete(long id);
	public List<User> getAllUser();
	public Object isValidUser(String username, String password);
	public Object getUser(String username);
	
}
