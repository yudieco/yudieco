package br.com.tutorial.springmvc.dao.hibernate;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tutorial.springmvc.dao.DAOAdmin;
import br.com.tutorial.springmvc.model.Admin;
import br.com.tutorial.springmvc.model.User;

@Transactional(propagation=Propagation.SUPPORTS)
@Repository("daoAdmin")
public class HBAdmin extends HBDAO<Admin> implements DAOAdmin {

	protected Class<Admin> getClazz() {
		return Admin.class;
	}
	
	public Admin get(String username, String password) {
		Query query = getSession().createQuery("from Admin where username = ? and password = ?");
		query.setString(0, username);
		query.setString(1, DigestUtils.sha256Hex(password));
		query.setString(1,password);
		return (Admin) query.uniqueResult();				   
	}		

	public void delete(long id) {
		Query query = getSession().createQuery("from Admin where id = ?");
		query.setLong(0, id);
		getSession().delete((Admin)query.uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUser() {
		Query query = getSession().createQuery("from User where permission.id <> 0 order by 'asc'");
		return query.list();
	}
	
	public Object isValidUser(String username, String password) {
		Query query = getSession().createQuery("from User where username = ? and password = ?");
		query.setString(0, username);
		query.setString(1, password);
		return query.uniqueResult();				   
	}
	
	public Object getUser(String username) {
		Query query = getSession().createQuery("from User where username = ?");
		query.setString(0, username);
		return query.uniqueResult();
	}
	
}
