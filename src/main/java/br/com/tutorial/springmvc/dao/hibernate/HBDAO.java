package br.com.tutorial.springmvc.dao.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tutorial.springmvc.dao.DAOBase;

@Transactional(propagation=Propagation.SUPPORTS)
public abstract class HBDAO<T> implements DAOBase<T> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return getSessionFactory().getCurrentSession();
	}
	
	@SuppressWarnings("rawtypes")
	protected abstract Class getClazz();
	
	
	public void save(T objeto) {
		getSession().saveOrUpdate(objeto);
	}
	
	public void delete(T objeto) {
		getSession().delete(objeto);
	}
	
	@SuppressWarnings("unchecked")
	public T get(Long id) {
		return (T) getSession().get(getClazz(), id);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> list() {
		return (List<T>) getSession().createCriteria(getClazz()).list();
	}
	
}
