package br.com.tutorial.springmvc.dao;

import java.util.List;

import br.com.tutorial.springmvc.model.Customer;

public interface DAOCustomer extends DAOBase<Customer> {
	public void delete(long id);
	public List<Customer> getAll();
	public boolean usernameIsValid(String username);
}
