package br.com.tutorial.springmvc.dao;

import java.util.List;

import br.com.tutorial.springmvc.model.Gift;

public interface DAOGift extends DAOBase<Gift> {

	public Gift get(int id);
	public void delete(long id);
	public List<Gift> getAll();

}