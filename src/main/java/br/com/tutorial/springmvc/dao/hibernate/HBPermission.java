package br.com.tutorial.springmvc.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.tutorial.springmvc.dao.DAOPermission;
import br.com.tutorial.springmvc.model.Permission;

@Transactional(propagation=Propagation.SUPPORTS)
@Repository("daoPermission")
public class HBPermission extends HBDAO<Permission> implements DAOPermission{

	@Override
	protected Class<Permission> getClazz() {
		return Permission.class;
	}

	public Permission get(String name) {
		Query query = getSession().createQuery("from Permission where role = ?");
		query.setString(0, name);
		return (Permission) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Permission> getAll() {
		Query query = getSession().createQuery("from Permission order by 'asc'");
		return query.list();
	}
	
	public void delete(long id) {
		Query query = getSession().createQuery("from Permission where id = ?");
		query.setLong(0, id);
		getSession().delete((Permission)query.uniqueResult());
	}
	
	
}
