package br.com.tutorial.springmvc.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.tutorial.springmvc.dao.DAOAdmin;
import br.com.tutorial.springmvc.model.User;

@Component
public class UserBO {
	
	@Autowired
	private DAOAdmin daoAdmin;
	
	public Object isValidUser(String username, String password) {
		Object object = daoAdmin.isValidUser(username, password);
		return object;
	}
	
	public long getTokenId(String username) {
		Object o = daoAdmin.getUser(username);
		return ((User)o).getId();
	}
	
}
