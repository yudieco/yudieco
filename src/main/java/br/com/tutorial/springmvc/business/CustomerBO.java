package br.com.tutorial.springmvc.business;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.tutorial.springmvc.dao.DAOAdmin;
import br.com.tutorial.springmvc.dao.DAOCustomer;
import br.com.tutorial.springmvc.dao.DAOPermission;
import br.com.tutorial.springmvc.model.Customer;
import br.com.tutorial.springmvc.model.Permission;

@Component
public class CustomerBO {

	public static String EMPTY_VALUE = "NULLVALUE";
	
	public static String USERNAME_EXIST = "USERNAMEEXIST";
	
	public static String CORRECT = "OK";

	@Autowired
	private DAOCustomer daoCustomer;

	@Autowired
	private DAOAdmin daoAdmin;

	@Autowired
	private DAOPermission daoPermission;

	@Autowired
	private UserBO userBO;

	public String saveCustomer(Customer customer) {
		Permission permission = daoPermission.get("NORMALACCOUNT");
		customer.setPassword(this.cripto(customer.getId(),
				customer.getPassword()));
		customer.setPermission(permission);

		if ( customer.getName().isEmpty() 
				|| customer.getAddress().isEmpty()
				|| customer.getSquare().isEmpty()
				|| customer.getComplement().isEmpty()
				|| customer.getCity().isEmpty()
				|| customer.getState().isEmpty()
				|| customer.getZipCode().isEmpty()
				|| customer.getUsername().isEmpty()
				|| customer.getPassword().isEmpty() ) {
			return EMPTY_VALUE;
		} else if (daoCustomer.usernameIsValid(customer.getUsername())) {
			return USERNAME_EXIST;
		} else {
			daoCustomer.save(customer);
			return CORRECT;
		}
	}

	public Customer getCustomer(long id) {
		return daoCustomer.get(id);
	}

	public void deleteCustomer(long id) {
		daoCustomer.delete(id);
	}

	public String cripto(long id, String password) {
		Customer customer = daoCustomer.get(id);
		String passwordNew = password;
		if (customer != null) {
			if (!password.equals(customer.getPassword())) {
				passwordNew = DigestUtils.sha256Hex(password);
			}
		} else {
			passwordNew = DigestUtils.sha256Hex(password);
		}
		return passwordNew;
	}

}