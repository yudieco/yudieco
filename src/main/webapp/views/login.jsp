<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Spring MVC</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<!-- Form area -->
<div class="admin-form">
  <div class="container">

    <div class="row">
      <div class="col-md-5">
        <!-- Widget starts -->
            
            <div class="widget worange">
              <!-- Widget head -->

              <div class="widget-head">
                Login
              </div>

              <div class="widget-content">
                <div class="padd">
                  <!-- Login form -->
				  <form class="form-horizontal" action="<c:url value="/j_spring_security_check"/>" method="post">
                    <!-- Email -->
                    <div class="form-group">
                      <label class="control-label col-lg-3" for="username">Username</label>
                      <div class="col-lg-9">
                        <input class="form-control" placeholder="Username" name="j_username" id="username">
                      </div>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                      <label class="control-label col-lg-3" for="password">Password</label>
                      <div class="col-lg-9">
                          <input type="password" class="form-control" id="password" name="j_password" placeholder="Password">
                      </div>
                    </div>
                    <!-- Remember me checkbox and sign in button -->
                    <div class="form-group">
					<div class="col-lg-9 col-lg-offset-3">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
						</div>
					</div>
					</div>
                        <div class="col-lg-9 col-lg-offset-3">
							<button type="submit" class="btn btn-danger">Sign in</button>
							<button type="reset" class="btn btn-default">Reset</button>
						</div>
                    <br />
                  </form>
				  
				</div>
                </div>
                    
                <div class="widget-foot">
                  Not Registred? <a href="#">Register here</a>
                </div>
            </div>  
      </div>
    </div>
  </div> 
</div>
	
		

<!-- JS --> 
<script type="text/javascript" src="<c:url value="/_js/jquery.js"/>"></script>
<script src="<c:url value="/_js/bootstrap.js"/>"></script>

</body>
</html>
