<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>"
	type="image/x-icon">
<link rel="icon" href="<c:url value="/images/favicon.ico"/>"
	type="image/x-icon">

<title>Yudi Games</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">


<!-- Custom CSS -->
<link rel="stylesheet" href="<c:url value="/css/shop-homepage.css"/>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a href="/springmvc/"> <img alt="Yudi Games"
					src="<c:url value="images/icone.png"/>">
				</a>
			</div>
			</br> </br>
			<div style="text-align: right;">
				<a href="createAccount" style="color: #fff">Criar conta</a> <span
					style="color: #fff">|</span> <a href="premiumAccount"
					style="color: #fff">Conta Premium</a>
			</div>

		</div>
		<!-- /.container -->
	</nav>

	<!-- Page Content -->
	<div class="container">

		<div class="row">

			<div class="col-md-3">
				<br> <br>
				<p class="lead">Categorias</p>
				<div class="list-group">
					<a href="<c:url value="listProductToSite/RPG"/>"
						class="list-group-item">RPG</a> <a
						href="<c:url value="listProductToSite/ACAO"/>"
						class="list-group-item">A��o</a> <a
						href="<c:url value="listProductToSite/OUTROS"/>"
						class="list-group-item">Outros</a>
				</div>
			</div>

			<div class="col-md-9">


				<div class="col-xs-5 center-block">

				<sf:form id="formCadastro" method="post" modelAttribute="giftBuy" role="form" >
						<h1>Yudi Games</h1>
					
						<div class="form-group">
									<div style="border-style: solid;">
										<img src="http://rafaeltome.dominiotemporario.com/scrummaster/${productRegistration.url}">
									</div>
									<br> <label for="exampleInputEmail1">Nome: ${productRegistration.name}</label>
									<br> <label for="exampleInputEmail1">Valor: ${productRegistration.price}</label> 
								
								<br> <label for="exampleInputEmail1">Nome da conta que receber� o presente</label> 
								<sf:input type="hidden" class="form-control" id="product" path="product.id" value="${productRegistration.id}" />
								<sf:input type="email" class="form-control" id="email" placeholder="email" path="email" />
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Cart�o de cr�dito</label>
							<div class="row">
								<div class="col-xs-8">
									<sf:input type="text" class="form-control" id="number" placeholder="numero" maxlength="16" path="creditcard"/>
								</div>
								<div class="col-xs-3">
									<sf:input type="text" class="form-control" id="securecode" placeholder="" maxlength="3" path="secureCode"/>
								</div>

							</div>
							<br>
							<div class="row col-xs-8">
								<button type="submit" class="btn btn-default " aria-label="Left Align">
									<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> 
									<a href="#">Comprar</a>
								</button>
							</div>
						</div>
					</sf:form>

				</div>
			</div>

		</div>
	</div>

	</div>
	<!-- /.container -->

	<div class="container">

		<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="container">
					<p>Copyright &copy; Yudi Games 2015</p>
				</div>
			</div>
		</footer>

	</div>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="<c:url value="/js/jquery.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
</body>

</html>