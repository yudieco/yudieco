<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><tiles:getAsString name="titulo" /></title>
    	<link href="<c:url value="/_css/bootstrap.css"/>" rel="stylesheet">
    	<link href="<c:url value="/_css/track.css"/>" rel="stylesheet">
	    
  </head>
  <body>

    <div id="wrapper">
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"">
    
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">


        <sec:authorize ifAnyGranted="Physician">
          <ul class="nav navbar-nav side-nav" style="background-color:white;">
			<li>
              <a href="<c:url value="/doctor/#"/>">
                <i class="icon-table"></i> Patient <span class="pull-right"><i class="icon-angle-right"></i></span>
              </a>
            </li>
			<li>
              <a href="<c:url value="/doctor/#"/>">
                <i class="icon-table"></i> Report <span class="pull-right"><i class="icon-angle-right"></i></span>
              </a>
            </li>
          </ul>
        </sec:authorize>
        
        <sec:authorize ifAnyGranted="Administrative">
	      <ul class="nav navbar-nav side-nav">
	        <li>
	        	<a href="<c:url value="/admin/listProduct"/>">
	            	<i class="icon-edit"></i> Produto <span class="pull-right"><i class="icon-angle-right"></i></span>
	            </a>
	        </li>
		 </ul>	          
		</sec:authorize>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown" style="color:#49d1c6; letter-spacing:1px;">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <sec:authentication property="details.fullName"/></a>
            </li>
            <li>
              <a href="<c:url value="/j_spring_security_logout"/>"><i class="icon-power-off"></i> Log Out</a>
            </li>
          </ul>          
        </div><!-- /.navbar-collapse -->
      </nav>

     	<div id="page-wrapper" style="background-color:#F8F9FA;">
			<div>
				<!-- Insercao dinamica do conte�do Tom� -->
				<tiles:insertAttribute name="content"/>
			</div>
		</div>
    </div><!-- /#wrapper -->

  </body>
</html>

