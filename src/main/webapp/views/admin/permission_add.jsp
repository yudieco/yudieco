<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>

<link href="<c:url value="../_css/fullcalendar.css"/>" rel="stylesheet">
<link href="<c:url value="../_css/bootstrap-datetimepicker.min.css"/>" rel="stylesheet">
<!-- Page Specific CSS -->
<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">        
<link rel="stylesheet" href="<c:url value="../_css/ui-lightness/jquery-ui-1.10.3.custom.css"/>">

<!-- Bootstrap core JavaScript -->
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="<c:url value="../_js/bootstrap.js"/>"></script>
<script src="<c:url value="../_js/jquery-ui-1.10.3.custom.min.js"/>"></script>
<script src="<c:url value="../_js/jquery.validate.min.js"/>"></script>
        
<div class="row">
	<div class="col-lg-12">
		<h1></h1>
		<ol class="breadcrumb">
			<li class="active"><a href="../admin/listPermission"><i class="icon-table"></i>List</a></li>
			<li class="active"><i class="icon-user"></i>Add Permission</li>
		</ol>
	</div>
</div><!-- /.row -->

<div class="row">
	<sf:form id="form_permission" method="post" modelAttribute="permissionRegistration" role="form" >
		<div class="col-lg-6">
			<div class="form-group">
			<sf:hidden class="form-control" id="id" path="id" placeholder="id"/>
			<sf:input  type="text" class="form-control" id="role" path="role" placeholder="role"/>
			</div>
			<button type="submit" class="btn btn-primary btn-lg" id="btnSalvaCadastro">Submit</button>    
		</div>
	</sf:form>
</div>