<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-lg-12">
    	<h1> <small></small></h1>
        <ol class="breadcrumb">
			<li class="active"><i class="icon-table"></i>List</li>
	        <li class="active"><a href="../admin/addPermission"><i class="icon-user"></i>Add Permission</a></li>
        </ol>
    </div>
</div><!-- /.row -->

<div class="row">
	<div class="col-lg-12">         
		<div class="table-responsive">
			<table class="table table-bordered table-hover table-striped">
				<thead>
				<tr class="alert alert-dismissable alert-success">
				<th class="alert alert-dismissable alert-success">Role <i class="icon-sort"></i></th>
				<th class="alert alert-dismissable alert-success">Action<i class="icon-sort"></i></th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="permission" items="${permissionList}">
					<tr>
						<td>${permission.role}</td>
						<td>
							<a href="../admin/deletePermission/${permission.id}"><i class="icon-eraser"></i> Delete</a>
							&nbsp;&nbsp;<a href="../admin/editPermission/${permission.id}"><i class="icon-table"></i> Edit</a>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>       
</div><!-- /.row -->

<!-- Bootstrap core JavaScript -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<c:url value="../../_js/bootstrap.js"/>"></script>    
<!-- Page Specific Plugins -->
<script src="<c:url value="../../_js/tablesorter/jquery.tablesorter.js"/>"></script>    
<script src="<c:url value="../../_js/tablesorter/tables.js"/>"></script>