<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>"
	type="image/x-icon">
<link rel="icon" href="<c:url value="/images/favicon.ico"/>"
	type="image/x-icon">

<title>Yudi Games</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">


<!-- Custom CSS -->
<link rel="stylesheet" href="<c:url value="/css/shop-homepage.css"/>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a href="/springmvc/"> <img alt="Yudi Games"
					src="<c:url value="images/icone.png"/>">
				</a>
			</div>
			</br>
			</br>
			<div style="text-align: right;">
				<a href="createAccount" style="color: #fff">Criar conta</a> <span
					style="color: #fff">|</span> <a href="premiumAccount"
					style="color: #fff">Conta Premium</a>
			</div>

		</div>
		<!-- /.container -->
	</nav>

	<!-- Page Content -->
	<div class="container">

		<div class="row">

			<div class="col-md-3">
				<br> <br>
				<p class="lead">Categorias</p>
				<div class="list-group">
					<a href="<c:url value="listProductToSite/RPG"/>"
						class="list-group-item">RPG</a> <a
						href="<c:url value="listProductToSite/ACAO"/>"
						class="list-group-item">A��o</a> <a
						href="<c:url value="listProductToSite/OUTROS"/>"
						class="list-group-item">Outros</a>
				</div>
			</div>

			<div class="col-md-9">
				<br> <br> <br>

				<div class="col-xs-12 col-md-4">
					<div class="panel panel-primary" style="border-color:#965A38">
						<div class="panel-heading" style="background-color:#965A38; border-color:#965A38">
							<h3 class="panel-title">Bronze</h3>
						</div>
						<div class="panel-body">
							<div class="the-price">
								<h1>
									R$ 5<span class="subscript">/m�s</span>
								</h1>
								<small>1 m�s FREE trial</small>
							</div>
							<table class="table">
								<tr>
									<td>1 conta</td>
								</tr>
								<tr class="active">
									<td>Descontos em todos os lan�amentos</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
						</div>
						<div class="panel-footer">
							<span class="pull-right">1 m�s FREE trial</span>
							<form target="pagseguro" method="post"
								action="https://pagseguro.uol.com.br/checkout/checkout.jhtml">
								<input type="hidden" name="email_cobranca"
									value="rafaeltsouza@terra.com.br" /> 
									<input type="hidden" name="tipo" value="CBR" /> 
									<input type="hidden" name="moeda" value="BRL" /> 
									<input type="hidden" name="item_id" value="10007" /> 
									<input type="hidden" name="item_descr" value="Conta Bronze - R$ 5,00/m�s" /> 
									<input type="hidden" name="item_quant" value="1" /> 
									<input type="hidden" name="item_valor" value="5,00" /> 
									<input type="hidden" name="frete" value="0" /> 
									<input type="hidden" name="peso" value="0" /> 
									<input type="image" name="submit"
									src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-comprar-assina.gif"
									alt="Pague com PagSeguro - � r�pido, gr�tis e seguro!" />
							</form>
						</div>
					</div>
				</div>


				<div class="col-xs-12 col-md-4">
					<div class="panel panel-primary" style="border-color:#CCC2C2">
						<div class="cnrflash">
							<div class="cnrflash-inner">
								<span class="cnrflash-label">MAIS <br>POPULAR</span>
							</div>
						</div>

						<div class="panel-heading" style="background-color:#CCC2C2; border-color:#CCC2C2">
							<h3 class="panel-title">Prata</h3>
						</div>
						<div class="panel-body">
							<div class="the-price">
								<h1>
									R$ 10<span class="subscript">/m�s</span>
								</h1>
								<small>1 m�s FREE trial</small>
							</div>
							<table class="table">
								<tr>
									<td>1 conta</td>
								</tr>
								<tr class="active">
									<td>Descontos em todos os lan�amentos</td>
								</tr>
								<tr>
									<td>Vantagem nas filas de jogos</td>
								</tr>
							</table>
						</div>
						<div class="panel-footer">
							<span class="pull-right">1 m�s FREE trial</span>
							<form target="pagseguro" method="post"
								action="https://pagseguro.uol.com.br/checkout/checkout.jhtml">
									<input type="hidden" name="email_cobranca" value="rafaeltsouza@terra.com.br">
									<input type="hidden" name="tipo" value="CBR" /> 
									<input type="hidden" name="moeda" value="BRL" /> 
									<input type="hidden" name="item_id" value="10008" /> 
									<input type="hidden" name="item_descr" value="Conta Prata - R$ 10,00/m�s" /> 
									<input type="hidden" name="item_quant" value="1" /> 
									<input type="hidden" name="item_valor" value="10,00" /> 
									<input type="hidden" name="frete" value="0" /> 
									<input type="hidden" name="peso" value="0" /> 
									<input type="image" name="submit"
									src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-comprar-assina.gif"
									alt="Pague com PagSeguro - � r�pido, gr�tis e seguro!" />
							</form>
						</div>
					</div>
				</div>


				<div class="col-xs-12 col-md-4">
					<div class="panel panel-primary" style="border-color:#C98910">
						<div class="panel-heading" style="background-color:#C98910; border-color:#C98910">
							<h3 class="panel-title">Ouro</h3>
						</div>
						<div class="panel-body">
							<div class="the-price">
								<h1>
									R$ 15<span class="subscript">/m�s</span>
								</h1>
								<small>1 m�s FREE trial</small>
							</div>
							<table class="table">
								<tr>
									<td>2 conta</td>
								</tr>
								<tr class="active">
									<td>Descontos em todos os lan�amentos</td>
								</tr>
								<tr>
									<td>Vantagem nas filas de jogos</td>
								</tr>
							</table>
						</div>
						<div class="panel-footer">
							<span class="pull-right">1 m�s FREE trial</span>
							<form target="pagseguro" method="post"
								action="https://pagseguro.uol.com.br/checkout/checkout.jhtml">
									<input type="hidden" name="email_cobranca" value="rafaeltsouza@terra.com.br">
									<input type="hidden" name="tipo" value="CBR" /> 
									<input type="hidden" name="moeda" value="BRL" /> 
									<input type="hidden" name="item_id" value="10009" /> 
									<input type="hidden" name="item_descr" value="Conta Ouro - R$ 15,00/m�s" /> 
									<input type="hidden" name="item_quant" value="1" /> 
									<input type="hidden" name="item_valor" value="15,00" /> 
									<input type="hidden" name="frete" value="0" /> 
									<input type="hidden" name="peso" value="0" /> 
									<input type="image" name="submit"
									src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-comprar-assina.gif"
									alt="Pague com PagSeguro - � r�pido, gr�tis e seguro!" />
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	</div>
	<!-- /.container -->

	<div class="container">

		<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="container">
					<p>Copyright &copy; Yudi Games 2015</p>
				</div>
			</div>
		</footer>

	</div>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="<c:url value="/js/jquery.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
</body>

</html>